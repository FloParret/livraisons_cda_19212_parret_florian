package afpa.exception;

public class ValeurInferieureException extends Exception {

	/**
	 * @author Florian Laurent Mustapha
	 */
	private static final long serialVersionUID = 1L;

	public ValeurInferieureException(String message) {
		super(message);
	}

}
