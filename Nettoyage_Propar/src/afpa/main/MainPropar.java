package afpa.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import afpa.personnel.Apprenti;
import afpa.personnel.Employe;
import afpa.personnel.Expert;
import afpa.personnel.Senior;

/**
 * @author Florian Laurent Mustapha
 *
 */
public class MainPropar {

	public static void main(String[] args) {
		Employe e1 = new Expert("Jean", "Charles", "Jean", "admin");
		Employe e2 = new Apprenti("Regis", "Bob", "Regis", "admin");
		Employe e3 = new Senior("Depardieu", "Gerard", "Gerard", "admin");

		menu();
	}

	/**
	 * Affichage et gestion du menu principal
	 */
	static public void menu() {

		String choix;
		Scanner sc = new Scanner(System.in);
		System.out.println(
				"\n-------------------------------    MENU NETTOYAGE PROPAR   ------------------------------------------------");
		System.out.println("(A) - Afficher la liste des operations en cours\r\n"
				+ "(B) - Afficher la liste des operations terminees\r\n" + "(C) - Connexion\r\n" + "(Q) - Quitter\r\n");
		choix = sc.next();
		choix = choix.toUpperCase();
		switch (choix) {
		case "A":
			Employe.listerOperationsEnCoursParNom();
			menu();
			break;
		case "B":
			Employe.listerOperationsFiniesParNom();
			menu();
			break;
		case "C":
			seConnecter();

			break;
		case "Q":

			System.out.println("\nAu revoir et a bientot chez Propar !");

			System.exit(0);

			break;
		default:
			System.out.println("\nCette commande n'existe pas... Retour au menu !\n");
			menu();
			break;
		}

	}

	/**
	 * Affichage et gestion du menu apprenti et senior
	 */
	static public void menuSpecifique(Employe e) {

		String choix;
		Scanner sc = new Scanner(System.in);
		System.out.println(
				"\n-------------------------------    MENU NETTOYAGE PROPAR   ------------------------------------------------\n\n");
		System.out.println("Bonjour " + e.getLogin() + "\n\n");
		System.out.println("(A) - Mes operations en cours\r\n" + "(B) - Prendre une operation\r\n"
				+ "(C) - Terminer une operation\r\n" + "(D) - Deconnexion\r\n");

		choix = sc.next();
		choix = choix.toUpperCase();
		switch (choix) {
		case "A":
			e.consulterMesOperationsEnCours();
			menuSpecifique(e);
			break;
		case "B":
			if (e.calculerMesOperationsEnCours() < e.getNbTachesMax()) {
				e.ajouterOperation();
			} else {
				System.out
						.println("Nombre maximum de taches en cours atteinte. Veuillez terminer vos taches d'abord.\n");
			}

			menuSpecifique(e);
			break;
		case "C":
			Employe.cloturerOperation(e);
			menuSpecifique(e);
			break;
		case "D":
			menu();

			break;
		default:
			System.out.println("\nCette commande n'existe pas... Retour au menu !\n");
			menuSpecifique(e);
			break;
		}

	}

	/**
	 * Affichage et gestion du menu expert
	 */
	static public void menuSpecifiqueExpert(Employe e) {

		String choix;
		Scanner sc = new Scanner(System.in);
		System.out.println(
				"\n-------------------------------    MENU NETTOYAGE PROPAR   ------------------------------------------------\n\n");
		System.out.println("Bonjour " + e.getLogin() + "\n\n");
		System.out.println("(A) - Mes operations en cours\r\n" + "(B) - Prendre une operation\r\n"
				+ "(C) - Terminer une operation\r\n" + "(D) - Ajouter un nouvel employe\r\n"
				+ "(E) - Consulter le total des recettes\r\n" + "(F) - Deconnexion\r\n");

		choix = sc.next();
		choix = choix.toUpperCase();
		switch (choix) {
		case "A":
			e.consulterMesOperationsEnCours();
			menuSpecifiqueExpert(e);
			break;
		case "B":
			if (e.calculerMesOperationsEnCours() < e.getNbTachesMax()) {
				e.ajouterOperation();
			} else {
				System.out
						.println("Nombre maximum de taches en cours atteinte. Veuillez terminer vos taches d'abord.\n");
			}

			menuSpecifiqueExpert(e);
			break;
		case "C":
			Employe.cloturerOperation(e);
			menuSpecifiqueExpert(e);
			break;
		case "D":
			Expert.addEmploye();
			menuSpecifiqueExpert(e);
			break;
		case "E":
			Operation.afficherTotalRecettes();
			menuSpecifiqueExpert(e);

			break;
		case "F":
			System.out.println("\n\nAu revoir " + e.getNom() + "\n\n");
			menu();

			break;
		default:
			System.out.println("\nCette commande n'existe pas... Retour au menu !\n");
			menuSpecifiqueExpert(e);
			break;
		}

	}

	/**
	 * Gestion de la connexion
	 */
	public static void seConnecter() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Entrez votre login");
		String login = scan.nextLine();
		System.out.println("Entrez votre password");
		String password = scan.nextLine();
		for (Employe e : Employe.employes) {
			if (login.equals(e.getLogin()) && password.equals(e.getPassword())) {
				System.out.println("\nVous etes connectes en tant que " + e.getLogin());
				if (Expert.class.isAssignableFrom(e.getClass()))
					menuSpecifiqueExpert(e);
				else {
					menuSpecifique(e);
				}
			}

		}
		System.out.println("Login ou Mot de passe errone. Voulez vous reessayer ? (O/N)");
		String reponse = scan.nextLine();
		reponse = reponse.toUpperCase();
		if (reponse.equals("O")) {
			seConnecter();
		} else {
			System.out.println("Retour au menu !\n\n");
			menu();
		}

	}
}
