package afpa.exception;

public class ValeurSuperieureException extends Exception {

	/**
	 * @author Florian Laurent Mustapha
	 */
	private static final long serialVersionUID = 1L;

	public ValeurSuperieureException(String message) {
		super(message);
	}

}
