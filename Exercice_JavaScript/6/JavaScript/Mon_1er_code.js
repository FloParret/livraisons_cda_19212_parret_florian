
var btnStart = document.getElementById('start');
var btnStop = document.getElementById('stop');
var btnPause = document.getElementById('pause');
var span = 	document.getElementById('span');
var tpsEcoule = 0;


(function(){
    btnStart.paramTps = tpsEcoule;
    btnPause.paramTps = tpsEcoule;
    btnStart.addEventListener("click",startChrono,false);
})()

function startChrono(e){
    /*btnStart.removeEventListener("click", startChrono,false);*/
    btnStart.onclick = null;
    btnStart.style.visibility = 'hidden';   
    btnStop.style.visibility = 'visible'; 
    btnPause.style.visibility = 'visible';
// algo de calcul de nombre haures, minutes et secondes écoulées
var startTime = new Date();
decompte = setInterval(function() {
    // 1- Convertir en secondes :
    var seconds = Math.round(
    (new Date().getTime() - startTime.getTime()) / 1000
    + e.target.paramTps); // e représente l'event déclencheur
    // e.target représente l'objet déclencheur
    // ici : bouton start ou bouton pause
    // (cette prop a été ajoutée aux boutons)
    // 2- Extraire les heures:
    var hours = parseInt( seconds / 3600 );
    seconds = seconds % 3600; // secondes restantes
    // 3- Extraire les minutes:
    var minutes = parseInt( seconds / 60 );
    seconds = seconds % 60; // secondes restantes
    // 4- afficher dans le span
    span.innerHTML = hours
    +":"+minutes
    +":"+seconds;
    // 5- incrémenter le nombre de secondes
    tpsEcoule += 1;
    }, 1000); // fin de function anonyme dans setInterval()

}

    

