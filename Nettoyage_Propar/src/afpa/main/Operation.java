package afpa.main;

import java.util.ArrayList;
import java.util.List;

import afpa.personnel.Employe;

/**
 * @author Florian Laurent Mustapha
 *
 */
public class Operation {
	private String type;
	private int prix;
	private String nomClient;
	private String prenomClient;
	private String commentaire;
	private boolean estTermine;
	private static List<Operation> operations = new ArrayList<Operation>();
	private Employe employe;

	public Operation() {
		super();

	}

	/**
	 * @param nomClient
	 * @param prenomClient
	 * @param type
	 * @param prix
	 * @param employe
	 */
	public Operation(String nomClient, String prenomClient, String commentaire, String type, int prix, Employe employe) {
		super();
		this.type = type;
		this.prix = prix;
		this.nomClient = nomClient;
		this.prenomClient = prenomClient;
		this.commentaire = commentaire;
		this.estTermine = false;
		this.employe = employe;
		operations.add(this);

	}

	/**
	 * @param type
	 * @param prix
	 */
	public Operation(String type, int prix) {
		super();
		this.type = type;
		this.prix = prix;

	}

	public String getType() {
		return type;
	}

	public String getNomClient() {
		return nomClient;
	}

	public String getPrenomClient() {
		return prenomClient;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public boolean isEstTermine() {
		return estTermine;
	}

	public void setEstTermine(boolean estTermine) {
		this.estTermine = estTermine;
	}

	/**
	 * affiche le total des recettes
	 */
	public static void afficherTotalRecettes() {
		long somme = 0;
		for (Operation op : operations) {
			if (op.estTermine) {
				somme = somme + op.getPrix();
			}
		}
		System.out.println("\nLe total des operations terminees est de " + somme + " euros");
	}

	@Override
	public String toString() {
		String isTermine = (estTermine)?"oui":"non";
		return "Type " + type + ", Prix " + prix + " euros. Client : " + nomClient + " " + prenomClient 
				+ ". Employe en charge : " + employe.getNom() + ". La tache est-elle terminee ?  " + isTermine + ".\n\t\t L'employ� a laiss� le commentaire suivant : \"" + commentaire + "\"."; 
	}
}
