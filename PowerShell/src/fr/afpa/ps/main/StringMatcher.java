package fr.afpa.ps.main;

import java.util.regex.Pattern;

/**
 * Cette classe permet l'attribution specifique de la commande adequate au choix
 * de l'utilisateur, par un systeme de regex.
 * 
 * @author Flo & Brahim
 * @version 1.0
 */
public class StringMatcher {



	Pattern pattern1NavigationCmdRegex = Pattern.compile("^(MyCd) ([A-Za-z]:)?([\\\\/])?([\\w-]+[\\\\/]?)+");
	Pattern pattern2NavigationCmdRegex = Pattern.compile("^(MyCd) \\.\\.");
	Pattern pattern3NavigationCmdRegex = Pattern.compile("^(MyCd)( ~)?");

	Pattern pattern1MvCmdRegex = Pattern.compile(
			"^(MyMv) ((([A-Za-z]:)?[\\\\/])?([\\w-]+[\\\\/])+)?\\w+\\.\\w+ (([A-Za-z]:)?[\\\\/])?([\\w-]+[\\\\/]?)+$");
	Pattern pattern2MvCmdRegex = Pattern.compile("^(MyMv) (\\w*\\/)*\\w+\\.\\w+ (\\w*\\/)*\\w+\\.\\w+");


	Pattern pattern1CpCmdRegex = Pattern
	.compile("^(MyCp) (([A-Za-z]:)?[\\\\/])?([\\w-]+[\\\\/]?)?\\w+\\.\\w+ (([A-Za-z]:)?[\\\\/])?([\\w-]+[\\\\/]?)+$");

	Pattern pattern2CpCmdRegex = Pattern
			.compile("^(MyCp) -r (([A-Za-z]:)?[\\\\/])?([\\w-]+[\\\\/]?)+ (([A-Za-z]:)?[\\\\/])?([\\w-]+[\\\\/]?)+$");

	Pattern pattern1FindCmdRegex = Pattern.compile("^(MyFind)( -R)? \\*[\\w\\.-]+");
	Pattern pattern2FindCmdRegex = Pattern.compile("^(MyFind)( -R)? [\\w\\.-]+\\*");
	Pattern pattern3FindCmdRegex = Pattern.compile("^(MyFind)( -R)? \\*[\\w\\.-]+\\*");

	Pattern pattern1RmCmdRegex = Pattern.compile("^(MyRm) \\*\\.\\w+");
	Pattern pattern2RmCmdRegex = Pattern.compile("^(MyRm)( (([A-Za-z]:)?[\\\\/])?([\\w-]+[\\\\/]?)*\\w+\\.\\w+)+");
	Pattern pattern3RmCmdRegex = Pattern.compile("^(MyRm) -rf (([A-Za-z]:)?[\\\\/])?([\\w-]+[\\\\/]?)+");

	Pattern pattern1DisplayCmdRegex = Pattern.compile("^(MyDisplay) (([A-Za-z]:)?[\\\\/])?([\\w*\\\\/-])+\\w+\\.\\w+");
	Pattern pattern2DisplayCmdRegex = Pattern.compile("^(MyDisplay) \\w+\\.\\w+");

	Pattern pattern1HelpCmdRegex = Pattern.compile("^(MyHelp)");

	/**
	 * Cette methode permet de differencié les differentes syntaxes de la commande
	 * MyCd afin de lui attribuer le traitement approprié.
	 * 
	 * @see cdCmd()
	 * 
	 */
	public int selectNavigationCmd(String s) {
		if (pattern1NavigationCmdRegex.matcher(s).matches()) {
			return 1;
		} else if (pattern2NavigationCmdRegex.matcher(s).matches()) {
			return 2;
		} else if (pattern3NavigationCmdRegex.matcher(s).matches()) {
			return 3;
		}
		System.out.println("Option de MyCd non reconnu..");
		return -1;

	}

	/**
	 * Cette methode permet de differencié les differentes syntaxes de la commande
	 * MyMv afin de lui attribuer le traitement approprié.
	 * 
	 * @see mvCmd()
	 * 
	 */

	public int selectMvCmd(String s) {
		if (pattern1MvCmdRegex.matcher(s).matches()) {
			return 1;
		} else if (pattern2MvCmdRegex.matcher(s).matches()) {
			return 2;
		}
		System.out.println("Option de MyMv non reconnu..");
		return -1;

	}

	/**
	 * Cette methode permet de differencié les differentes syntaxes de la commande
	 * MyCp afin de lui attribuer le traitement approprié.
	 * 
	 * @see cpCmd()
	 * 
	 */

	public int selectCpCmd(String s) {
		if (pattern1CpCmdRegex.matcher(s).matches()
				&& Application.tokens.get(1).toString().matches("(([A-Za-z]:[\\\\/])?([\\w-]+[\\\\/])+)?\\w+\\.\\w+")) {
			return 1;
		} else if (pattern2CpCmdRegex.matcher(s).matches()) {
			return 2;
		}
		System.out.println("Option de MyCp non reconnu..");
		return -1;

	}

	/**
	 * Cette methode permet de differencié les differentes syntaxes de la commande
	 * MyFind afin de lui attribuer le traitement approprié.
	 * 
	 * @see findCmd()
	 * 
	 */

	public int selectFindCmd(String s) {
		if (pattern1FindCmdRegex.matcher(s).matches()) {
			return 1;
		} else if (pattern2FindCmdRegex.matcher(s).matches()) {
			return 2;
		} else if (pattern3FindCmdRegex.matcher(s).matches()) {
			return 3;
		}
		System.out.println("Option de MyFind non reconnu..");
		return -1;

	}

	/**
	 * Cette methode permet de differencié les differentes syntaxes de la commande
	 * MyRm afin de lui attribuer le traitement approprié.
	 * 
	 * @see rmCmd()
	 * 
	 */

	public int selectRmCmd(String s) {
		if (pattern1RmCmdRegex.matcher(s).matches()) {
			return 1;
		} else if (pattern2RmCmdRegex.matcher(s).matches()) {
			return 2;
		} else if (pattern3RmCmdRegex.matcher(s).matches()) {
			return 3;
		}

		System.out.println("Option de MyRm non reconnu..");
		return -1;

	}

	/**
	 * Cette methode permet de differencié les differentes syntaxes de la commande
	 * MyDisplay afin de lui attribuer le traitement approprié.
	 * 
	 * @see displayCmd()
	 * 
	 */

	public int selectDisplayCmd(String s) {
		if (pattern1DisplayCmdRegex.matcher(s).matches()) {
			return 1;

		}
		if (pattern2DisplayCmdRegex.matcher(s).matches()) {
			return 1;

		}

		System.out.println("Option de Mydisplay non reconnu..");
		return -1;

	}

}