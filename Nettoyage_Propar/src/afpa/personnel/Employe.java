package afpa.personnel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import afpa.exception.ValeurInferieureException;
import afpa.exception.ValeurSuperieureException;
import afpa.main.Operation;

/**
 * 
 * @author Florian Laurent Mustapha
 *
 */
public class Employe {
	private String nom;
	private String prenom;
	private String login;
	private String password;
	private int id;
	private int nbTachesMax;
	private static int idCpt = 0;

	public List<Operation> operations = new ArrayList<Operation>();
	public static List<Employe> employes = new ArrayList<>();

	static Scanner scan = new Scanner(System.in);

	public Employe() {
		super();
		this.id = Employe.idCpt++;
		Employe.employes.add(this);
	}

	/**
	 * @param nom
	 * @param prenom
	 * @param login
	 * @param password
	 */
	public Employe(String nom, String prenom, String login, String password) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.password = password;
		this.id = Employe.idCpt++;
		Employe.employes.add(this);
	}

	public int getNbTachesMax() {
		return nbTachesMax;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public void setNbTachesMax(int nbTachesMax) {
		this.nbTachesMax = nbTachesMax;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	@Override
	public String toString() {

		return "L'employe s'appelle " + nom + " " + prenom + ", son ID est le #" + id
				+ ". Il est capable de realiser simultanement " + nbTachesMax + " tache(s) au maximum.";
	}

	public static void confirmationAjoutOperation() {

		System.out.println("\nL'operation a bien ete ajoutee a votre liste");

	}

	/**
	 * Ajout d'une opération si cela est possible après demande des caractéristiques
	 * de celle-ci
	 */
	public void ajouterOperation() {
		System.out.println("AJOUT D'UNE OPERATION");
		System.out.println("Entrez le nom du client : ");
		String nom = scan.nextLine();
		System.out.println("Entrez le prenom du client : ");
		String prenom = scan.nextLine();
		System.out.println("Veuillez saisir un commentaire sur l'operation :");
		String commentaire = scan.nextLine();
		String type = "";
		boolean erreur = true;
		Operation nouvelleOperation;
		do {
			System.out.println("Entrez le type d'operation (P -> Petite, M -> Moyenne, G -> Grande, A -> Autre)");
			type = scan.nextLine().toUpperCase();
			switch (type) {
			case "P":
				nouvelleOperation = new Operation(nom, prenom, commentaire,"Petite", 1000, this);
				this.operations.add(nouvelleOperation);
				confirmationAjoutOperation();
				erreur = false;
				break;
			case "M":
				nouvelleOperation = new Operation(nom, prenom, commentaire,"Moyenne", 2500, this);
				this.operations.add(nouvelleOperation);
				confirmationAjoutOperation();
				erreur = false;
				break;
			case "G":
				nouvelleOperation = new Operation(nom, prenom, commentaire,"Grande", 10000, this);
				this.operations.add(nouvelleOperation);
				confirmationAjoutOperation();
				erreur = false;
				break;
			case "A":
				System.out.println("Vous avez choisi une operation personnalisee.\n");
				System.out.println("Veuillez saisir le prix de cette operation : \n");
				String s;
				int prix = 0;
				boolean notEntier = true;
				do {
					s = scan.nextLine();
					try {
						prix = lireClavier(s, 1, 2_000_000_000);
						notEntier = false;
					} catch (ValeurInferieureException e) {
						System.out.println(e.getMessage());
					} catch (ValeurSuperieureException e) {
						System.out.println(e.getMessage());
					} catch (Exception e) {
						System.out
								.println("Il faut entrer un prix de valeur entiere compris entre 1 et 2 000 000 000 !");
					}
				} while (notEntier);
				nouvelleOperation = new Operation(nom, prenom, commentaire,"Personnalisee", prix, this);
				this.operations.add(nouvelleOperation);
				confirmationAjoutOperation();
				erreur = false;
				break;
			default:
				System.out.println("Entrez un type valide (P, M, G ou A)");
				break;
			}
		} while (erreur);

	}

	/**
	 * Affiche les opérations en Cours
	 * 
	 * @return Liste des opérations en Cours
	 */
	public List consulterMesOperationsEnCours() {
		System.out.println("\nMES OPERATIONS EN COURS : ");
		List<Operation> opFinies = new ArrayList<Operation>();
		for (int i = 0; i < operations.size(); i++) {
			if (!operations.get(i).isEstTermine()) {
				opFinies.add(operations.get(i));
			}
		}
		if (opFinies.size() > 0) {
			for (int i = 0; i < opFinies.size(); i++) {
				System.out.println("\t" + (i + 1) + " - " + opFinies.get(i));
			}
		} else {
			System.out.println("Vous n'avez aucune operation en cours.\n");
		}
		return opFinies;
	}

	/**
	 * @param String saisie au clavier
	 * @param int    min
	 * @param int    max
	 * @return String s pars� en entier entre min et max inclus
	 * @throws ValeurInferieureException
	 * @throws ValeurSuperieureException
	 */
	public static int lireClavier(String s, int min, int max)
			throws ValeurInferieureException, ValeurSuperieureException {
		if (Integer.parseInt(s) < min) {
			throw new ValeurInferieureException("Le nombre saisi est trop petit");
		} else if (Integer.parseInt(s) > max) {
			throw new ValeurSuperieureException("Le nombre saisi est trop grand");
		}
		return Integer.parseInt(s);
	}

	/**
	 * Affiche les operations en cours de l'employe et cloture l'operation souhaitee
	 * 
	 * @param employe
	 */
	public static void cloturerOperation(Employe employe) {
		List<Operation> opFinies = employe.consulterMesOperationsEnCours();
		if (opFinies.size() > 0) {
			System.out.println("Entrez le numero de l'operation que vous souhaitez cloturer : ");
			boolean notEntier = true;
			String s;
			int numOperation = 0;
			do {
				s = scan.nextLine();
				try {
					numOperation = lireClavier(s, 1, opFinies.size());
					notEntier = false;
				} catch (ValeurInferieureException e) {
					System.out.println(e.getMessage());
				} catch (ValeurSuperieureException e) {
					System.out.println(e.getMessage());
				} catch (Exception e) {
					System.out.println("Il faut entrer un chiffre compris entre 1 et " + opFinies.size() + " !");
				}
			} while (notEntier);
			opFinies.get(numOperation - 1).setEstTermine(true);
			System.out.println("\nL'operation a ete cloturee");
		}

	}

	/**
	 * @return nb d'operations en cours
	 */
	public int calculerMesOperationsEnCours() {
		int cpt = 0;
		for (int i = 0; i < operations.size(); i++) {
			if (!operations.get(i).isEstTermine())
				cpt++;
		}
		return cpt;
	}

	/**
	 * Tri par ordre alphabétique de nom puis prenom de la liste d'employes
	 */
	public static void trierListeEmploye() {
		Collections.sort(employes, new Comparator<Employe>() {
			@Override
			public int compare(Employe e1, Employe e2) {
				if (e1.getNom().compareTo(e2.getNom()) == 0) {

					return e1.getPrenom().compareTo(e2.getPrenom());
				} else {
					return e1.getNom().compareTo(e2.getNom());
				}
			}
		});
	}

	/**
	 * Affiche les operations en cours triees par nom
	 */
	public static void listerOperationsEnCoursParNom() {
		Employe.trierListeEmploye();

		System.out.println("\nAFFICHAGE DE TOUTES LES OPERATIONS EN COURS :");
		for (Employe employe : employes) {
			System.out.println(" - " + employe.nom + " " + employe.prenom + " ("
					+ employe.calculerMesOperationsEnCours() + " operations en cours)");
			for (Operation operation : employe.operations) {
				if (!operation.isEstTermine()) {
					System.out.println("\t- " + operation);
				}
			}
		}

	}

	/**
	 * Affiche les operations terminees triees par nom
	 */
	public static void listerOperationsFiniesParNom() {
		Employe.trierListeEmploye();
		System.out.println("\nAFFICHAGE DE TOUTES LES OPERATIONS TERMINEES :");
		for (Employe employe : employes) {
			System.out.println(" - " + employe.nom + " " + employe.prenom + " ("
					+ (employe.operations.size() - employe.calculerMesOperationsEnCours()) + " operations terminees)");
			for (Operation operation : employe.operations) {
				if (operation.isEstTermine()) {
					System.out.println("\t- " + operation);
				}
			}

		}

	}
}