package afpa.personnel;

import java.util.Scanner;

public class Expert extends Employe {

	/**
	 * 
	 * @author Florian Laurent Mustapha
	 *
	 */

	static Scanner scan = new Scanner(System.in);

	public Expert() {
		super();
	}

	/**
	 * @param nom
	 * @param prenom
	 * @param login
	 * @param password
	 */
	public Expert(String nom, String prenom, String login, String password) {
		super(nom, prenom, login, password);
		this.setNbTachesMax(5);
	}

	/**
	 * Ajout d'un nouvel employe dans le programme
	 */
	public static void confirmationAjoutEploye(Employe e) {
		System.out.println("\nL'employé " + e.getNom() + " " + e.getPrenom() + " a bien été ajouté à votre liste");
	}

	/**
	 * @return Login (boucle tant que le login fait doublon)
	 */
	public static String verifDoublonLogin() {
		System.out.println("Entrez le login de l'employé : ");
		String s = scan.nextLine();
		for (Employe employe : employes) {
			if (employe.getLogin().equals(s)) {
				System.out.println("Ce login fait doublon, veuillez en choisir un autre.");
				s = verifDoublonLogin();
			}
		}
		return s;
	}

	/**
	 * Ajout d'un employe (nom, prenom, login, password et type)
	 */
	public static void addEmploye() {
		System.out.println("AJOUT D'UN NOUVEL EMPLOYE");
		System.out.println("Entrez le nom de l'employé : ");
		String nom = scan.nextLine();
		System.out.println("Entrez le prenom de l'employé : ");
		String prenom = scan.nextLine();
		String login = verifDoublonLogin();
		System.out.println("Entrez le password de l'employé : ");
		String password = scan.nextLine();
		String statut = "";
		boolean erreur = true;
		Employe nouvelEmploye;
		do {
			System.out.println("Entrez le statut de l'employé (E -> Expert, S -> Senior, A -> Apprenti)");
			statut = scan.nextLine().toUpperCase();
			switch (statut) {
			case "E":
				nouvelEmploye = new Expert(nom, prenom, login, password);
				confirmationAjoutEploye(nouvelEmploye);
				erreur = false;
				break;
			case "S":
				nouvelEmploye = new Senior(nom, prenom, login, password);
				confirmationAjoutEploye(nouvelEmploye);
				erreur = false;
				break;
			case "A":
				nouvelEmploye = new Apprenti(nom, prenom, login, password);
				confirmationAjoutEploye(nouvelEmploye);
				erreur = false;
				break;
			default:
				System.out.println("Entrez un statut valide (E, S ou A)");
				break;
			}
		} while (erreur);

	}

}
