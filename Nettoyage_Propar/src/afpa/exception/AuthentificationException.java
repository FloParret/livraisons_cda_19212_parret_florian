package afpa.exception;

public class AuthentificationException extends Exception {

	/**
	 *  @author Florian Laurent Mustapha 
	 */
	private static final long serialVersionUID = 1L;

	public AuthentificationException() {
		super();
	}

	public AuthentificationException(String arg0) {
		super(arg0);
	}

}
